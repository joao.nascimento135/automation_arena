const { defineConfig } = require("cypress");

module.exports = defineConfig({
  chromeWebSecurity: false,
  requestTimeout: 9000,
  responseTimeout: 30000,
  projectId: 'u4qxtw',
  viewportWidth: 1600,
  viewportHeight: 900,
  env: {
    ARENA_PAGE: 'https://go.arena.im/chat/cesar/Xed5mok',
  },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    // specPattern: 'cypress/e2e/*/.feature',
  },
});
