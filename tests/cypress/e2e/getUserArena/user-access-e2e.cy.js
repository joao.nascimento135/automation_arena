
const common = require('../../fixtures/Pages/common-elements').elements
const ENDPOINT = Cypress.env("ARENA_PAGE");

describe("User access Arena Page", () => {
    beforeEach(() => cy.visit(ENDPOINT));
    it("TC001-Check customer login with valid credentials", () => {
        cy.checkLinkArena()
        cy.clicksettings()
        cy.clickLogin()
        cy.get('#modal', { timeout: 10000 }).should('not.exist');
        cy.fixture('userMass').then((userFixture) => {
            const user = userFixture.user.email
            const password = userFixture.user.password_ok
            cy.get(common.email).click({ force: true }).type(user)
            cy.get(common.continue).click({ force: true })
            const login = 'Login'
            cy.get(common.title_login).should("have.text", login)
            cy.get(common.password).click({ force: true }).type(password)
            cy.get(common.login_button).click({ force: true })
        })
        cy.checkLogin()
    })

    it("TC002-Check customer login with an invalid password", () => {
        cy.checkLinkArena()
        cy.clicksettings()
        cy.clickLogin()
        cy.get('#modal', { timeout: 10000 }).should('not.exist');
        cy.fixture('userMass').then((userFixture) => {
            const user = userFixture.user.email
            const password = userFixture.user.password_nok
            cy.get(common.email).click({ force: true }).type(user)
            cy.get(common.continue).click({ force: true })
            const login = 'Login'
            cy.get(common.title_login).should("have.text", login)
            cy.get(common.password).click({ force: true }).type(password)
            cy.get(common.login_button).click({ force: true })
            const incorrect_user = "E-mail or password incorrect"
            cy.get(common.nagtive_msg, { timeout: 10000 }).should('have.text', incorrect_user)

        })
    })

    it("TC003-Send message", () => {
        cy.checkLinkArena()
        cy.clicksettings()
        cy.clickLogin()
        cy.loginSuccess()
        const send_msg = "sending a message from cypress"
        cy.clicksettings()
        cy.clickOnChat().type(send_msg)
        cy.get(common.send_msg_button).click({ force: true })
        cy.get(common.check_sent_msg).last().should("have.text", send_msg)
    })

    it("TC004-React to a message", () => {
        cy.get(8000)
        cy.get(common.check_sent_msg_nl, {timeout: 10000}).last().trigger('mouseover')
        cy.get(common.heart).last().click({ force: true });
    })

    it("TC005-Send a message with emoji", () => {
        cy.checkLinkArena()
        cy.clicksettings()
        cy.clickLogin()
        cy.loginSuccess()
        cy.clicksettings()
        cy.clickOnChat()
        cy.get(common.emoji).click({ force: true })
        cy.get(common.emoji_smile).click({ force: true })
        cy.get(common.send_msg_button).click({ force: true })
        cy.wait(1000)
        cy.get(common.sent_emoji)
            .last()
            .should('be.visible')
            .should(([img]) => {
                expect(img.naturalWidth).to.equal(72);
                expect(img.naturalHeight).to.equal(72);
            })
            .then(([img]) => {
                cy.fixture('images/emoji.png').then((content1) => {
                    const fixtureImage = new Image();
                    fixtureImage.src = `data:Fotos_1/jpeg;base64,${content1}`;
                    fixtureImage.onload = () => {
                        expect(img.naturalWidth).to.equal(fixtureImage.naturalWidth);
                        expect(img.naturalHeight).to.equal(fixtureImage.naturalHeight);
                    };
                });
            });
    })
})