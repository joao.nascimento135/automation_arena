export const elements = {
    
    link_arena_page: '[class="arena-header--container"] a',
    settings: '[class="style__ChatRoomHeaderMenuContainer-PdYYx ZQRpt chat-room--chat--header--menu--container"]',
    login: '[class="arena-dropdown-menu"] li div',
    email: '[autocomplete="email"]',
    continue: '[class="arena-btn"]',
    title_login: '[class="live-login-email--title"]',
    password: '[type="password"]',
    login_button: '[class="live-login-second--form--btn"]',
    login_user: '[class="live-event-login--user"]',
    nagtive_msg: '.arena-form--error',
    chat: '[class="react-input-emoji--wrapper"]',
    send_msg_button: '[class="arena-icon-send"]',
    msg_today: '[class="style__ChatRoomMessageDayContainer-jImjIS ehFLDY arena-chat-widget--message-day-container]',
    check_sent_msg: '[class="style__ChatRoomMessageContentText-deWULh iryJtw arena-chat-widget--message-content-text-user"]',
    check_sent_msg_nl: '[class="style__ChatRoomMessageContentText-deWULh dkOeRl arena-chat-widget--message-content-text"]',
    heart: '[class="style__ChatRoomMessageContentReactionsIconReactHeart-etHhbi FXTZD arena-chat-widget--message-content-reactions-icon-react-heart-off"]',
    emoji: '[class="react-input-emoji--button--icon"]',
    emoji_smile: '[aria-label="😀, grinning"]',
    sent_emoji: '[src="https://twemoji.maxcdn.com/2/72x72/1f600.png"'
}
