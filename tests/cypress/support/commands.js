// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
const common = require('../fixtures/Pages/common-elements').elements

Cypress.Commands.add("checkLinkArena", () => {
    cy.get(common.link_arena_page).should('be.visible')
});

Cypress.Commands.add("clicksettings", () => {
    cy.get(common.settings).click({force:true})
});
Cypress.Commands.add("clickLogin", () => {
    cy.get(common.login).contains('Login').click({force:true})
});
Cypress.Commands.add("checkLogin", () => {
    cy.get(common.login_user).should('be.visible')
});
Cypress.Commands.add("loginSuccess", () => {
    cy.get('#modal', { timeout: 10000 }).should('not.exist');
    cy.fixture('userMass').then((userFixture) => {
        const user = userFixture.user.email
        const password = userFixture.user.password_ok
        cy.get(common.email).click({ force: true }).type(user)
        cy.get(common.continue).click({ force: true })
        const login = 'Login'
        cy.get(common.title_login).should("have.text", login)
        cy.get(common.password).click({ force: true }).type(password)
        cy.get(common.login_button).click({ force: true })
    })
});
Cypress.Commands.add("clickOnChat", () => {
    cy.get(common.chat).click({force:true})
});

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })